#include "fivewin.ch"

function main()
local oWnd
local oVmenu
local oItem
local oIcon

DEFINE ICON oIcon NAME "C5ICON"


DEFINE WINDOW oWnd TITLE "First test VistaMenu" ICON oIcon

    oVMenu := TVistaMenu():New( 0,0, 100, 100, oWnd )
    oVMenu:nColumns := 1
    oVMenu:nType := 2

    oItem := oVMenu:AddItem( "Sistema y mantenimiento ", "..\..\images\image1.png" )
             oItem:AddItem( "Empezar a trabajar con windows" )
             oItem:AddItem( "Hacer una copia de seguridad del equipo" )
             oItem:AddItem( "Item 1 Item 1" )
             oItem:AddItem( "ItemItem2 ItemItem2" )
             oItem:AddItem( "ItemItemItem3 ItemItemItem3" )

    oItem := oVMenu:AddItem( "Seguridad", "..\..\images\image2.png" )
             oItem:AddItem( "Buscar actualizaciones" )
             oItem:AddItem( "Dejar pasar un programa a través de Firewall de Windows" )

    oItem := oVMenu:AddItem( "Redes e Internet", "..\..\images\image3.png" )
    oItem:lEnable := .f.
             oItem:AddItem( "Ver el estado y las tareas de red" )
             oItem:AddItem( "Configurar el uso compartido de archivos" )

    oItem := oVMenu:AddItem( "Hardware y sonido", "..\..\images\image4.png" )
             oItem:AddItem( "Reproducir un CD u otros archivos multimedia automáticamente" )
             oItem:AddItem( "Impresora" )
             oItem:AddItem( "Mouse" )

    oItem := oVMenu:AddItem( "Programas", "..\..\images\image5.png" )
             oItem:AddItem( "Desinstalar un programa" )
             oItem:AddItem( "Cambiar programas de inicio" )

    oItem := oVMenu:AddItem( "Equipo portatil", "..\..\images\image6.png" )
             oItem:AddItem( "Cambiar la configuración de la bateria" )
             oItem:AddItem( "Ajustar parametros de configuración de movilidad de uso frecuente" )

    oItem := oVMenu:AddItem( "Cuentas de usuario", "..\..\images\image7.png" )
             oItem:AddItem( "Cambiar tipo de cuenta" )

    oItem := oVMenu:AddItem( "Opciones adicionales", "..\..\images\image12.png" )

    oItem := oVMenu:AddItem( "Apariencia y personalización", "..\..\images\image8.png" )
             oItem:AddItem( "Cambiar fondo de escritorio" )
             oItem:AddItem( "Cambiar la combinación de colores" )
             oItem:AddItem( "Ajustar la resolución de pantalla" )

    oItem := oVMenu:AddItem( "Reloj, idioma y región", "..\..\images\image9.png" )
             oItem:AddItem( "Cambiar teclados u otros métodos de entrada" )
             oItem:AddItem( "Cambiar el idioma para mostrar" )

    oItem := oVMenu:AddItem( "Accesibilidad", "..\..\images\image10.png" )
             oItem:AddItem( "Permitir que Windows sugiera parametros de configuración" )
             oItem:AddItem( "Optimizar la presentación visual" )

    oWnd:oClient := oVMenu

ACTIVATE WINDOW oWnd

return 0
