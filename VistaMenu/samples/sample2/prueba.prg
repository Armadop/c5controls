#include "fivewin.ch"

function main()
local oWnd
local oVmenu
local oItem
local oIcon
local oFont, oFont2

DEFINE ICON oIcon NAME "C5ICON"

   DEFINE FONT oFont  NAME "Segoe UI" SIZE 0, -20
   DEFINE FONT oFont2 NAME "Segoe UI" SIZE 0, -16


DEFINE WINDOW oWnd TITLE "Second test VistaMenu" ICON oIcon FROM 155, 162 TO 640, 1035 PIXEL

    oVMenu := TVistaMenu():New( 0,0, 100, 100, oWnd, oFont, oFont2 )
    oVMenu:nColumns := 2
    oVMenu:SetMarginH( 30 )
    oVMenu:nWLeftImage := 100

    oItem := oVMenu:AddItem( "Seguridad", "..\..\images2\image2.bmp", {||MsgInfo(time())} )
    oItem:SetTooltip( "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.","..\..\images2\phone.bmp","Seguridad")

             oItem:AddItem( "Buscar actualizaciones",, {|o|MsgInfo(o:cText)} ):cToolTip := "Item buscar actualizaciones"
             oItem:AddItem( "Dejar pasar un programa a través de Firewall de Windows" )

    oItem := oVMenu:AddItem( "Redes e Internet", "..\..\images2\image3.bmp", {|| InsertarItem( oVMenu, 4 )} )
    oItem:cToolTip := "ipsum lorem ipsum lorem ipsum"
             oItem:AddItem( "Ver el estado y las tareas de red" )
             oItem:AddItem( "Configurar el uso compartido de archivos" )

    oItem := oVMenu:AddItem( "Hardware y sonido", "..\..\images2\image4.bmp" )
             oItem:AddItem( "Reproducir un CD u otros archivos multimedia automáticamente" )
             oItem:AddItem( "Impresora" )
             oItem:AddItem( "Mouse" )

    oItem := oVMenu:AddItem( "Programas", "..\..\images2\image5.bmp" )
             oItem:AddItem( "Desinstalar un programa" )
             oItem:AddItem( "Cambiar programas de inicio" )

    oItem := oVMenu:AddItem( "Equipo portatil", "..\..\images2\image6.bmp" )
             oItem:AddItem( "Cambiar la configuración de la bateria" )
             oItem:AddItem( "Ajustar parametros de configuración de movilidad de uso frecuente" )

    oItem := oVMenu:AddItem( "Cuentas de usuario", "..\..\images2\image7.bmp" )
             oItem:AddItem( "Cambiar tipo de cuenta" )

    oItem := oVMenu:AddItem( "Apariencia y personalización", "..\..\images2\image8.bmp" )
             oItem:AddItem( "Cambiar fondo de escritorio" )
             oItem:AddItem( "Cambiar la combinación de colores" )
             oItem:AddItem( "Ajustar la resolución de pantalla" )

    oItem := oVMenu:AddItem( "Reloj, idioma y región", "..\..\images2\image9.bmp" )
             oItem:AddItem( "Cambiar teclados u otros métodos de entrada" )
             oItem:AddItem( "Cambiar el idioma para mostrar" )


    oWnd:oClient := oVMenu



ACTIVATE WINDOW oWnd

return 0

function InsertarItem( o, nEn )

o:InsertItem( nEn, "Prueba de insertar","..\..\images2\image8.bmp",{|oitem|MsgInfo("Insertado", oItem:cText) } )

return 0


