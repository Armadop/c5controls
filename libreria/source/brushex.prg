#include "fivewin.ch"

/** Class to make a brush from a manufactured bitmap.
    The bitmat can be made with a dimension and two colors and a degrade vertical or horizontal
    if not provide the second color, the bitmap will be a color flat.
    The user he will be resposible to destroy the brush if is not the syste that makes it */

CLASS TBrushEx

      DATA   hBrush, hBitmap
      METHOD New( nWidth, nHeight, nColor, nColor2, lVGrad ) CONSTRUCTOR
      METHOD NewEx( nWidth, nHeight, nColor, nColor2, nColor3, nColor4, lVGrad ) CONSTRUCTOR
      METHOD End() INLINE DeleteObject(::hBrush),DeleteObject(::hBitmap)

ENDCLASS

**************************************************************************
  METHOD New( nWidth, nHeight, nColor, nColor2, lVGrad ) CLASS TBrushEx
**************************************************************************

    ::hBitmap := CreaBitmapEx(nWidth, nHeight, nColor, nColor2, lVGrad )
    ::hBrush = If( ::hBitmap != 0, CreatePatternBrush( ::hBitmap ), )

   //::nCount := 1

   //AAdd( ::aBrushes, Self )

return Self

**************************************************************************
  METHOD NewEx( nWidth, nHeight, nColor, nColor2, nColor3, nColor4, lVGrad ) CLASS TBrushEx
**************************************************************************

    ::hBitmap := CreaBitmapEx2(nWidth, nHeight, nColor, nColor2, nColor3, nColor4, lVGrad )
    ::hBrush = If( ::hBitmap != 0, CreatePatternBrush( ::hBitmap ), )

   //::nCount := 1

   //AAdd( ::aBrushes, Self )

return Self



