#include "fivewin.ch"



// Pasar un color de RGB a HSL    ( Red-Green-Blue  a  Hue-Saturation-Luminosity )
************************************************************************************
  function RGB2HSL( RGBColor )
************************************************************************************
return RGBtoHLS( RGBColor )


************************************************************************************
  function HSL2RGB( H, S, L )
************************************************************************************
return HLStoRGB( H, L, S )

************************************************************************************
  function HSV2RGB( hue, sat, val )
************************************************************************************
    local red, grn, blu
    local i, f, p, q, t

    hue %= 360
    if val == 0
       return nRGB( 0, 0, 0 )
    endif

    sat /=100
    val /=100
    hue /=60
    i := floor(hue)
    f := hue-i
    p := val*(1-sat)
    q := val*(1-(sat*f))
    t := val*(1-(sat*(1-f)))
    do case
       case i == 0
            red := val
            grn := t
            blu := p

       case i == 1
            red := q
            grn := val
            blu := p

       case i == 2
            red := p
            grn := val
            blu := t

       case i == 3
            red := p
            grn := q
            blu := val

       case i == 4
            red := t
            grn := p
            blu := val

       case i == 5
            red := val
            grn := p
            blu := q
    endcase

    red := floor(red*255)
    grn := floor(grn*255)
    blu := floor(blu*255)

return nRGB(red, grn, blu )


************************************************************************************
  function RGB2HSV( red, grn, blu )
************************************************************************************

    local  x, f, i, hue, sat, val

    red /= 255
    grn /= 255
    blu /= 255
    x   := min(min(red, grn), blu)
    val := max(max(red, grn), blu)

    if x == val
        return {0, 0, val*100}
    endif

    if red == x
       f := grn-blu
    else
       if grn == x
          f := blu-red
       else
          f := red-grn
       endif
    endif

    if red == x
       i := 3
    else
       if grn == x
          i := 5
       else
          i := 1
       endif
    endif

    hue := floor((i-f/(val-x))*60)%360
    sat := floor(((val-x)/val)*100)
    val := floor(val*100)

return {hue, sat, val}


//
//    Usage
//   h = 360;
//   s = 50;
//   v = 25;
//   trace("H"+h+" S"+s+" V"+v);
//   trace ("Converts to");
//   col = hsv2rgb(h, s, v);
//   r = col.r;
//   g = col.g;
//   b = col.b;
//   trace("R"+r+" G"+g+" B"+b);
//   trace("");
//   trace("R"+r+" G"+g+" B"+b);
//   trace ("Converts to");
//   col = rgb2hsv(r, g, b);
//   h = col.h;
//   s = col.s;
//   v = col.v;
//   trace ("H"+h+" S"+s+" V"+v);Hope it helps,
//
//-PiXELWiT


//' Example:
//' Colorize an image using the hue and saturation values
//' of the specified color.
//'
//Public Shared Function Colorize(ByVal source As Image, ByVal color As Color) As Bitmap
//   Dim fHue As Single = color.GetHue()
//   Dim fSat As Single = color.GetSaturation()
//   Dim dibTmp As New DIBitmap32(source)
//   '' The line above is the same as the line following...
//   'Dim dibTmp As DIBitmap32 = CType(source, DIBitmap32)
//   With dibTmp
//      For iX As Integer = 0 To .Width - 1
//         For iY As Integer = 0 To .Height - 1
//            .Pixel(iX, iY) = Pixel32.FromHSB(fHue, fSat, .Pixel(iX, iY).GetBrightness(), .Pixel(iX, iY).Alpha)
//         Next
//      Next
//   End With
//   Return dibTmp ' <- widening conversion performed
//End Function



************************************************************************************
 function VarH_HLS( nRGB, nX )
************************************************************************************
local a := RGBtoHLS( nRGB )
local X := a[1]

X := X + nX

if X < 0
   X := 0
else
   if X > 255
      X := 255
   endif
endif

return HLStoRGB( X,a[2],a[3])

************************************************************************************
  function VarS_HLS( nRGB, nX )
************************************************************************************
local a := RGBtoHLS( nRGB )
local X := a[3]

X := X + nX

if X < 0
   X := 0
else
   if X > 255
      X := 255
   endif
endif

return HLStoRGB( a[1],a[2],X)

************************************************************************************
 function VarL_HLS( nRGB, nX )
************************************************************************************
local a := RGBtoHLS( nRGB )
local X := a[2]

X := X + nX

if X < 0
   X := 0
else
   if X > 255
      X := 255
   endif
endif

return HLStoRGB( a[1],X, a[3])


************************************************************************************
  function VarH_HSV( nRGB, nX ) // 0-360
************************************************************************************
local a := RGB2HSV( GetRValue(nRGB), GetGValue(nRGB), GetBValue( nRGB ) )


a[1] := a[1] + nX

if a[1] > 360
   a[1] := 360
else
   if a[1] < 0
      a[1] := 0
   endif
endif

return HSV2RGB( a[1], a[2], a[3] )

************************************************************************************
  function VarS_HSV( nRGB, nX ) // 0-360
************************************************************************************
local a := RGB2HSV( GetRValue(nRGB), GetGValue(nRGB), GetBValue( nRGB ) )


a[2] := a[2] + nX

if a[2] > 360
   a[2] := 360
else
   if a[2] < 0
      a[2] := 0
   endif
endif

return HSV2RGB( a[1], a[2], a[3] )

************************************************************************************
  function VarV_HSV( nRGB, nX ) // 0-360
************************************************************************************
local a := RGB2HSV( GetRValue(nRGB), GetGValue(nRGB), GetBValue( nRGB ) )


a[3] := a[3] + nX

if a[3] > 360
   a[3] := 360
else
   if a[3] < 0
      a[3] := 0
   endif
endif

return HSV2RGB( a[1], a[2], a[3] )

************************************************************************************
  function Lighter( nRGB, nX )
************************************************************************************
local a := RGBtoHLS( nRGB )

return HLStoRGB( a[1],a[2]+nX,a[3])

************************************************************************************
  function Darken( nRGB, nX )
************************************************************************************
local a := RGBtoHLS( nRGB )

return HLStoRGB( a[1],a[2]-nX,a[3])

************************************************************************************
  function GetLum( nRGB )
************************************************************************************

return RGBtoHLS( nRGB )[2]

************************************************************************************
  function GetHue( nRGB )
************************************************************************************

return RGBtoHLS( nRGB )[1]

************************************************************************************
  function GetSaturation( nRGB )
************************************************************************************

return RGBtoHLS( nRGB )[3]

************************************************************************************
  function SetLum( nRGB, nValue )
************************************************************************************
local a := RGBtoHLS( nRGB )

return HLStoRGB( a[1], nValue, a[3] )

************************************************************************************
  function SetHue( nRGB, nValue )
************************************************************************************
local a := RGBtoHLS( nRGB )

return HLStoRGB( nValue, a[2], a[3] )

************************************************************************************
  function SetSaturation( nRGB, nValue )
************************************************************************************
local a := RGBtoHLS( nRGB )

return HLStoRGB( a[1], a[2], nValue )

************************************************************************************
function SetRed( nColor, nValue )
************************************************************************************
return   nRGB( nValue, GetGValue(nColor), GetBValue(nColor))

************************************************************************************
function SetGreen( nColor, nValue )
************************************************************************************
return   nRGB( GetRValue(nColor), nValue, GetBValue(nColor))

************************************************************************************
function SetBlue( nColor, nValue )
************************************************************************************
return   nRGB( GetRValue( nColor ), GetGValue(nColor, nValue ) )





#pragma BEGINDUMP
#include "windows.h"
#include "hbapi.h"
#include "wingdi.h"

/*VOID ColorRGBToHLS(          COLORREF clrRGB,
    WORD *pwHue,
    WORD *pwLuminance,
    WORD *pwSaturation
);
COLORREF ColorHLSToRGB(          WORD wHue,
    WORD wLuminance,
    WORD wSaturation
);
*/

HB_FUNC( RGBTOHLS )
{
    typedef void ( FAR PASCAL  *LPCOLORRGBTOHLS )( COLORREF, WORD*,WORD*,WORD* );

    WORD pwHue = 0;
    WORD pwLuminance = 0;
    WORD pwSaturation = 0;

    HINSTANCE hLib;
    LPCOLORRGBTOHLS ColorRGBToHLS;

    hLib = LoadLibrary( "shlwapi.dll" );
    if ( hLib )
    {
        ColorRGBToHLS = (LPCOLORRGBTOHLS) GetProcAddress( hLib, "ColorRGBToHLS" );
        ColorRGBToHLS( hb_parnl( 1 ), &pwHue, &pwLuminance, &pwSaturation );
    	FreeLibrary( hLib );
    }

    hb_reta( 3 );
    hb_storvni( pwHue, -1, 1 );
    hb_storvni( pwLuminance, -1, 2 );
    hb_storvni( pwSaturation, -1, 3 );
}

HB_FUNC( HLSTORGB )
{
   typedef COLORREF (FAR PASCAL * LPCOLORHLSTORGB ) (WORD, WORD, WORD );
   COLORREF color = 0;
   HINSTANCE hLib;
   LPCOLORHLSTORGB ColorHLSToRGB;
   hLib = LoadLibrary( "shlwapi.dll" );
   if ( hLib )
   {
       ColorHLSToRGB = (LPCOLORHLSTORGB) GetProcAddress( hLib, "ColorHLSToRGB" );
       color = ColorHLSToRGB( hb_parni( 1 ),hb_parni( 2 ),hb_parni( 3 ));
       FreeLibrary( hLib );
   }
   hb_retnl( color );
}
/*COLORREF ColorAdjustLuma(          COLORREF clrRGB,
    int n,
    BOOL fScale
);*/
// 0-100
HB_FUNC( BRIGHTNESS )
{
   typedef COLORREF (FAR PASCAL * LPCOLORADJUSTLUMA ) (COLORREF, int, BOOL );
   COLORREF color = hb_parnl( 1 );
   HINSTANCE hLib;
   LPCOLORADJUSTLUMA ColorAdjustLuma;
   hLib = LoadLibrary( "shlwapi.dll" );
   if ( hLib )
   {
       ColorAdjustLuma = (LPCOLORADJUSTLUMA) GetProcAddress( hLib, "ColorAdjustLuma" );
       color = ColorAdjustLuma( hb_parnl( 1 ),hb_parni( 2 ),hb_parl( 3 ));
       FreeLibrary( hLib );
   }
   hb_retnl( color );
}

HB_FUNC( GETRVALUE )
{
   hb_retni( GetRValue( (COLORREF) hb_parnl( 1 ) ) );
}
HB_FUNC( GETGVALUE )
{
   hb_retni( GetGValue( (COLORREF) hb_parnl( 1 ) ) );
}
HB_FUNC( GETBVALUE )
{
   hb_retni( GetBValue( (COLORREF) hb_parnl( 1 ) ) );
}

COLORREF MedColor( COLORREF sColor, COLORREF eColor )
{
   // Gradient params
   int height = 100;

   // Draw gradient

   double percent;
   unsigned char red, green, blue;
   COLORREF color;

   // Gradient color percent
   percent = 1 - (double)(height/2) / (double)(height-2);

   // Gradient color
   red   = (unsigned char)(GetRValue(sColor)*percent) + (unsigned char)(GetRValue(eColor)*(1-percent));
   green = (unsigned char)(GetGValue(sColor)*percent) + (unsigned char)(GetGValue(eColor)*(1-percent));
   blue  = (unsigned char)(GetBValue(sColor)*percent) + (unsigned char)(GetBValue(eColor)*(1-percent));
   color = RGB((COLORREF)red, (COLORREF)green, (COLORREF)blue);
   return color;

}

HB_FUNC( COLORMEDIO )
{
    hb_retnl( (long ) MedColor( (COLORREF) hb_parnl( 1 ), (COLORREF) hb_parnl( 2 ) ) );
}


COLORREF ColorPorciento( COLORREF sColor, COLORREF eColor, int nPorc )
{
   // Gradient params
   int height = 100;

   // Draw gradient

   double percent;
   unsigned char red, green, blue;
   COLORREF color;

   // Gradient color percent
   percent = 1 - (double) nPorc / (double) height;

   // Gradient color
   red   = (unsigned char)(GetRValue(sColor)*percent) + (unsigned char)(GetRValue(eColor)*(1-percent));
   green = (unsigned char)(GetGValue(sColor)*percent) + (unsigned char)(GetGValue(eColor)*(1-percent));
   blue  = (unsigned char)(GetBValue(sColor)*percent) + (unsigned char)(GetBValue(eColor)*(1-percent));
   color = RGB((COLORREF)red, (COLORREF)green, (COLORREF)blue);
   return color;

}


HB_FUNC( COLORPORCIENTO )
{
   COLORREF color1 = hb_parnl( 1 );
   COLORREF color2 = hb_parnl( 2 );
   int nPorc = hb_parni( 3 );
   hb_retnl( (long) ColorPorciento( color1, color2, nPorc ));
}

HB_FUNC( TOGRAY )
{
  COLORREF nColor = (COLORREF) hb_parnl( 1 );
  COLORREF nRojo  = GetRValue( nColor );
  COLORREF nVerde = GetGValue( nColor );
  COLORREF nAzul  = GetBValue( nColor );

  nColor = max( nRojo, max( nVerde, nAzul ) );

  hb_retnl( (long ) RGB( nColor, nColor, nColor) );
}

#pragma ENDDUMP
